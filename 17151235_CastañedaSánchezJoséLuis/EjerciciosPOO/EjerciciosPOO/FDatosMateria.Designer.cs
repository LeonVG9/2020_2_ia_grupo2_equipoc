﻿namespace EjerciciosPOO
{
    partial class FDatosMateria
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.TB_NombreMateria = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TB_ClaveMateria = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.NUD_Creditos = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_Creditos)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nombre de la materia";
            // 
            // TB_NombreMateria
            // 
            this.TB_NombreMateria.Location = new System.Drawing.Point(139, 20);
            this.TB_NombreMateria.Name = "TB_NombreMateria";
            this.TB_NombreMateria.Size = new System.Drawing.Size(136, 20);
            this.TB_NombreMateria.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Clave de la materia";
            // 
            // TB_ClaveMateria
            // 
            this.TB_ClaveMateria.Location = new System.Drawing.Point(139, 57);
            this.TB_ClaveMateria.Name = "TB_ClaveMateria";
            this.TB_ClaveMateria.Size = new System.Drawing.Size(136, 20);
            this.TB_ClaveMateria.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Créditos de la materia";
            // 
            // NUD_Creditos
            // 
            this.NUD_Creditos.Location = new System.Drawing.Point(139, 102);
            this.NUD_Creditos.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.NUD_Creditos.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.NUD_Creditos.Name = "NUD_Creditos";
            this.NUD_Creditos.Size = new System.Drawing.Size(70, 20);
            this.NUD_Creditos.TabIndex = 7;
            this.NUD_Creditos.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(102, 142);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(81, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Aceptar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(201, 142);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(74, 23);
            this.button2.TabIndex = 9;
            this.button2.Text = "Cancelar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // FDatosMateria
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(318, 196);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.NUD_Creditos);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TB_ClaveMateria);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TB_NombreMateria);
            this.Controls.Add(this.label1);
            this.Name = "FDatosMateria";
            this.Text = "Datos de la materia";
            ((System.ComponentModel.ISupportInitialize)(this.NUD_Creditos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TB_NombreMateria;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TB_ClaveMateria;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown NUD_Creditos;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}