﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace KMedias
{
    class LectorDatos
    {
        public int columnas = 0;
        public int filas = 0;
        public double distancia = 0;
        public string[] variables;
        string Texto;
        public double[,] Valores;
        public string[] registros;
        public double resultado;

        public LectorDatos(string pathFuente = "")
        {
            if (pathFuente.Length > 0)
            {
                LeerDatos(pathFuente);
            }
        }

        public void LeerDatos(string pathFuente)
        {
            registros = File.ReadAllLines(pathFuente);

            foreach (var LineaActual in registros)
            {
                variables = LineaActual.Split(',');
                columnas = variables.GetLength(0);
                filas++;
            }

            Valores = new double[filas, columnas];
            filas = 0;

            foreach (var LineaActual in registros)
            {
                variables = LineaActual.Split(',');
                for (int m = 0; m < registros.GetLength(0); m++)
                {
                    Valores[filas, m] = Convert.ToDouble(variables[m]);
                }
                filas++;
            }


            for (int i = 0; i < Valores.GetLength(0);i++)
            {
                for (int j = 0; j < Valores.GetLength(1);j++)
                {
                   Texto += Valores[i, j] + " ";
                }

                Texto += "\n";
            }   
        }

        public double calculaDistancia(int A, int B, string pathFuente)
        {
            registros = File.ReadAllLines(pathFuente);

            foreach (var LineaActual in registros)
            {
                variables = LineaActual.Split(',');
                columnas = variables.GetLength(0);
                filas++;
            }

            Valores = new double[filas, columnas];
            filas = 0;

            foreach (var LineaActual in registros)
            {
                variables = LineaActual.Split(',');
                for (int m = 0; m < registros.GetLength(0); m++)
                {
                    Valores[filas, m] = Convert.ToDouble(variables[m]);
                }
                filas++;
            }

            double[] ValoresA = new double[columnas];
            double[] ValoresB = new double[columnas];

            for (int i = 0; i < Valores.GetLength(0); i++)
            {
                for (int j = 0; j < Valores.GetLength(1); j++)
                {
                    if (i == (A - 1))
                    {
                        ValoresA[j] = Valores[i, j];
                    }
                    if (i == (B - 1))
                    {
                        ValoresB[j] = Valores[i, j];
                    }
                }
            }

            for (int i = 0; i < ValoresA.GetLength(0); i++)
            {
               distancia += Math.Pow((ValoresA[i] - ValoresB[i]),2);
            }

            resultado = Math.Sqrt(distancia);
            return resultado;

        }

        public string GetTexto()
        {
            return Texto;
        }
    }
}