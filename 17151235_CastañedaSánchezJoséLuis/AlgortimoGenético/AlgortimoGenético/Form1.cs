﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlgortimoGenético
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void BTN_Poblacion_Click(object sender, EventArgs e)
        {
            if (TB_Modelo.Text != "")
            {
                string Modelo = TB_Modelo.Text;
                char[] Caracter = Modelo.ToCharArray();
                int NumIndividuos = Convert.ToInt32(NUP_Poblacion.Value);
                string[] Individuos = new string[NumIndividuos];
                Random r = new Random();

                for (int j = 0; j < NumIndividuos; j++)
                {
                    for (int i = 0; i < Modelo.Length; i++)
                    {
                        RTB_Poblacion.Text += Caracter[r.Next(0, Modelo.Length)].ToString();
                    }

                    RTB_Poblacion.Text = "\n";
                }

            }
            else
            {
                MessageBox.Show("Ingrese un modelo a evaluar");
            }
        }
    }
}
