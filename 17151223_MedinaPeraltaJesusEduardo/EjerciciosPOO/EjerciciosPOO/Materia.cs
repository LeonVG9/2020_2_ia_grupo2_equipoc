﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjerciciosPOO
{
    public class Materia
    {
        string Clave;
        string Nombre;
        string Profesor;
        int Semestre;
        int Creditos;
        int Calificacion;

        int Intentos;

        public Materia(string Clave, string Nombre, int Creditos)
        {
            this.Clave = Clave;
            this.Nombre = Nombre;
            this.Creditos = Creditos;
            Calificacion = 0;
            Intentos = 1;
        }

        public string Cursar(string ClaveMateria, int Calificacion)
        {
            if (ClaveMateria != Clave)
            {
                return "No se encontró la materia";
            }
            if (YaEstaAprobada())
            {
                return "La materia ya se cursó y esta aprobada";
            }
            if (IntentosAgotados())
            {
                return "Ya alcanzaste el limite de intentos";
            }
            if (Calificacion < 70)
            {
                Calificacion = 0;
                Intentos++;
            }
            else
            {
                this.Calificacion = Calificacion;
            }
            return "";
        }

        public bool YaEstaAprobada()
        {
            return Calificacion > 69;
        }

        public bool YaEstaCursada()
        {
            return Intentos > 1 || Calificacion != 0;
        }

        public bool IntentosAgotados()
        {
            return Intentos > 3;
        }

        public int GetCalificacion()
        {
            return Calificacion;
        }

        public int GetCreditos()
        {
            return Creditos;
        }

        public string GetDatosMateria()
        {
            string datos = "";

            datos += Nombre + ", clave: " + Clave + ", créditos: " + Creditos;

            return datos;
        }
    }
}
