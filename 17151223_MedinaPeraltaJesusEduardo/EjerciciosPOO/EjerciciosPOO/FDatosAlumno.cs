﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EjerciciosPOO
{
    public partial class FDatosAlumno : Form
    {
        Alumno ElAlumno;
        public FDatosAlumno()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ElAlumno = new Alumno(TB_NombreAlumno.Text, TB_NumControl.Text);
            button2.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FDatosMateria VentanaMateria = new FDatosMateria();
            if (VentanaMateria.ShowDialog() == DialogResult.OK)
            {
                ElAlumno.AgregarMateria(VentanaMateria.GetMateria());
                RTB_DatosMaterias.Text = ElAlumno.GetDatosMaterias();
            }
        }

        private void BTN_Cursar_Click(object sender, EventArgs e)
        {
            string Error = ElAlumno.CursarMateria(TB_ClaveMateriaCursar.Text, (int)NUD_CalificacionMateriaCursada.Value);

            if (Error == "")
            {
                MessageBox.Show("Materia cursada");
                TB_Promedio.Text = ElAlumno.CalcularPromedio().ToString();
                TB_Creditos.Text = ElAlumno.CalcularCreditos().ToString();
            }
            else
            {
                MessageBox.Show("No fue posible cursar la materia " + Error);
            }
        }
    }
}
