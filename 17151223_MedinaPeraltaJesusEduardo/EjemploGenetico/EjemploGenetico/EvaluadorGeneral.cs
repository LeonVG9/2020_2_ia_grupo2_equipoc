﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemploGenetico
{
    class EvaluadorGeneral
    {
        public int TamGenotipo;
        public int LimInferiorFenotipo;
        public int LimSuperiorFenotipo;

        public virtual int Evaluar(Individuo individuo)
        {
            return 0;
        }
    }
}
