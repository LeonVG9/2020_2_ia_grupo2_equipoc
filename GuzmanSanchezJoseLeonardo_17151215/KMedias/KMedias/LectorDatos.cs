﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace KMedias
{
    class LectorDatos
    {
        String LineaActual;
        String PathFuente;
        StreamReader ArchivoFuente;
        StringBuilder Texto;

        public LectorDatos(string PathFuente = "")
        {
            if (PathFuente.Length > 0)
            {
                LeerDatos(PathFuente);
            }
        }

        public void LeerDatos(string PathFuente)
        {
            this.PathFuente = PathFuente;
            ArchivoFuente = new StreamReader(PathFuente);
            LineaActual = ArchivoFuente.ReadLine();

            Texto = new StringBuilder();

            while (LineaActual != null)
            {
                
                Texto.Append(LineaActual);
                Texto.Append("\n");
                LineaActual = ArchivoFuente.ReadLine();
            }

        }

        public string GetTexto()
        {
            return Texto.ToString();
        }
    }
}
