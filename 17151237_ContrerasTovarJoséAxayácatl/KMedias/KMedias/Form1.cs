﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KMedias
{
    public partial class Form1 : Form
    {
        LectorDatos miLectorDatos;
        public Form1()
        {
            InitializeComponent();
        }

        private void aBRIRToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OFDTexto.ShowDialog() == DialogResult.OK)
            {
                miLectorDatos = new LectorDatos(OFDTexto.FileName);

                RTBTextoArchivo.Text = miLectorDatos.GetTexto();
            }
        }

        private void LabelA_Click(object sender, EventArgs e)
        {

        }

        private void TB_Resultado_TextChanged(object sender, EventArgs e)
        {

        }

        private void BTN_Calcular_Click(object sender, EventArgs e)
        {
            int A = Convert.ToInt32(NUD_A);
            int B = Convert.ToInt32(NUD_B);

            LectorDatos calcula = new LectorDatos();
            calcula.calculaDistancia(A, B);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void aRCHIVOToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
