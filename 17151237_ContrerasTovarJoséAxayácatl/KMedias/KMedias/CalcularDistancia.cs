﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace KMedias
{
    class CalcularDistancia
    {
        public int columnas = 0;
        public int filas = 0;
        public string[] variables;
        public double[,] Valores;
        public string[] registros;
        public double[,] CopiaCluster;

        public void calculaDistancia(int numcluster, int numcolumna, double [,] Cluster, string pathFuente)
        {
            //Se hace lo mismo que en la clase LectorDatos, esto para obtener la matriz y los datos a trabajar.
            registros = File.ReadAllLines(pathFuente);

            foreach (var LineaActual in registros)
            {
                variables = LineaActual.Split(',');
                columnas = variables.GetLength(0);
                filas++;
            }

            Valores = new double[filas, columnas];
            filas = 0;

            foreach (var LineaActual in registros)
            {
                variables = LineaActual.Split(',');
                for (int m = 0; m < variables.GetLength(0); m++)
                {
                    Valores[filas, m] = Convert.ToDouble(variables[m]);
                }
                filas++;
            }

            bool CentrosIguales = false;

            CopiaCluster = new double[numcluster, numcolumna];

            do
            {
                for (int i = 0; i < numcluster; i++)
                {
                    for (int j = 0; i < numcolumna; j++)
                    {
                        Cluster[i, j] = CopiaCluster[i, j];
                    }
                }

                double[,] matComparacion = new double[1 * numcluster, numcolumna];
                double[,] matComparacion2 = new double[1, numcolumna];
                double[,] matContador = new double[1 + (numcolumna * 2), numcluster];

                double val1 = 0;
                double suma = 0;


                for (int i = 0; i < numcluster; i++)
                {
                    for (int j = 0; j < numcolumna; j++)
                    {

                        for (int x = 0; x < filas; x++)
                        {
                            val1 = Cluster[x, i] - Valores[j, x];
                            double val1Cuadrado = Math.Pow(val1, 2);
                            suma += val1Cuadrado;
                        }

                        double raiz = Math.Sqrt(suma);
                        suma = 0;
                        matComparacion[i, j] = raiz;
                    }
                }

                double chico = 1000;
                for (int i = 0; i < matComparacion.GetLength(1); i++)
                {
                    chico = 100;
                    for (int x = 0; x < matComparacion.GetLength(0); x++)
                    {
                        if (matComparacion[x, i] <= chico)
                        {
                            chico = matComparacion[x, i];
                            matComparacion2[0, i] = x;
                        }
                    }
                }




                for (int i = 0; i < matComparacion2.GetLength(1); i++)
                {

                    matContador[0, (int)matComparacion2[0, i]]++;


                    for (int x = 0; x < filas; x++)
                    {
                        matContador[x + 1, (int)matComparacion2[0, i]] += Valores[i, x];
                        matContador[(x + filas) + 1, (int)matComparacion2[0, i]] = matContador[x + 1, (int)matComparacion2[0, i]] / matContador[0, (int)matComparacion2[0, i]];
                    }


                }
            }
            while (CentrosIguales == false);

        }
    }
}
