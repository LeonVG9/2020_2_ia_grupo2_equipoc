﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace KMedias
{
    class LectorDatos
    {
        string pathFuente;
        int columnas = 0;
        int filas = 0;
        string[] variables;
        string Texto;
        double[,] Valores;
        double[,] ValoresA;
        double[,] ValoresB;

        public LectorDatos(string pathFuente = "")
        {
            if (pathFuente.Length > 0)
            {
                LeerDatos(pathFuente);
            }
        }

        public void LeerDatos(string pathFuente)
        {
            string[] registros = File.ReadAllLines(pathFuente);

            foreach (var LineaActual in registros)
            {
                variables = LineaActual.Split(',');
                columnas = variables.GetLength(0);
                filas++;
            }

            double[,] Valores = new double[filas, columnas];
            filas = 0;

            foreach (var LineaActual in registros)
            {
                variables = LineaActual.Split(',');
                for (int m = 0; m < registros.GetLength(0); m++)
                {
                    Valores[filas, m] = Convert.ToDouble(variables[m]);
                }
                filas++;
            }


            for (int i = 0; i < Valores.GetLength(0);i++)
            {
                for (int j = 0; j < Valores.GetLength(1);j++)
                {
                   Texto += Valores[i, j] + " ";
                }

                Texto += "\n";
            }   
        }

        public void calculaDistancia(int A, int B)
        {
            for (int i = 0; i < Valores.GetLength(0); i++)
            {
                for (int j = 0; j < Valores.GetLength(1); j++)
                {
                    if (i == (A - 1))
                    {
                        ValoresA[i,j] = Valores[i, j];
                    }
                    if (i == (B - 1))
                    {
                        ValoresB[i, j] = Valores[i, j];
                    }
                }
            }



        }

        public string GetTexto()
        {
            return Texto;
        }
    }
}