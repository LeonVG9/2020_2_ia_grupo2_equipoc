﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EjerciciosPOO
{
    public partial class FDatosAlumno : Form
    {
        Alumno ElAlumno;
        public FDatosAlumno()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ElAlumno = new Alumno(TB_NombreAlumno.Text, TB_NumControl.Text);
            button2.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FDatosMateria VentanaMateria = new FDatosMateria();
            if (VentanaMateria.ShowDialog() == DialogResult.OK)
            {
                ElAlumno.AgregarMateria(VentanaMateria.GetMateria());
                RTBDatosMateria.Text = ElAlumno.GetDatosMaterias();
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void BTCursarMateria_Click(object sender, EventArgs e)
        {
            string error = ElAlumno.CursarMateria(TBClaveMateriaCursada.Text, (int)NUDCalificacionMateriaCursada.Value);

            if (error == "")
            {
                MessageBox.Show("Materia cursada correctamente");

                LPromedio.Text = "Promedio: " + ElAlumno.CalcularPromedio();
                LCreditos.Text = "Creditos: " + ElAlumno.CalcularCreditos();
            }
            else
                MessageBox.Show("Error al cursar materia. " + error);

           

        }
    }
}
