﻿namespace EjerciciosPOO
{
    partial class FDatosAlumno
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TB_NombreAlumno = new System.Windows.Forms.TextBox();
            this.TB_NumControl = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.RTBDatosMateria = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TBClaveMateriaCursada = new System.Windows.Forms.TextBox();
            this.Calificacion = new System.Windows.Forms.Label();
            this.NUDCalificacionMateriaCursada = new System.Windows.Forms.NumericUpDown();
            this.BTCursarMateria = new System.Windows.Forms.Button();
            this.LPromedio = new System.Windows.Forms.Label();
            this.LCreditos = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.NUDCalificacionMateriaCursada)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre del alumno";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Número de control";
            // 
            // TB_NombreAlumno
            // 
            this.TB_NombreAlumno.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TB_NombreAlumno.Location = new System.Drawing.Point(172, 6);
            this.TB_NombreAlumno.Name = "TB_NombreAlumno";
            this.TB_NombreAlumno.Size = new System.Drawing.Size(236, 20);
            this.TB_NombreAlumno.TabIndex = 2;
            // 
            // TB_NumControl
            // 
            this.TB_NumControl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TB_NumControl.Location = new System.Drawing.Point(172, 45);
            this.TB_NumControl.Name = "TB_NumControl";
            this.TB_NumControl.Size = new System.Drawing.Size(236, 20);
            this.TB_NumControl.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button1.Location = new System.Drawing.Point(343, 79);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Ingresar Nuevo";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(343, 108);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(99, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Agregar Materia";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 154);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Materias ";
            // 
            // RTBDatosMateria
            // 
            this.RTBDatosMateria.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.RTBDatosMateria.Location = new System.Drawing.Point(102, 138);
            this.RTBDatosMateria.Name = "RTBDatosMateria";
            this.RTBDatosMateria.ReadOnly = true;
            this.RTBDatosMateria.Size = new System.Drawing.Size(340, 204);
            this.RTBDatosMateria.TabIndex = 7;
            this.RTBDatosMateria.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 374);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Materia a Cursar";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // TBClaveMateriaCursada
            // 
            this.TBClaveMateriaCursada.Location = new System.Drawing.Point(102, 371);
            this.TBClaveMateriaCursada.Name = "TBClaveMateriaCursada";
            this.TBClaveMateriaCursada.Size = new System.Drawing.Size(106, 20);
            this.TBClaveMateriaCursada.TabIndex = 9;
            // 
            // Calificacion
            // 
            this.Calificacion.AutoSize = true;
            this.Calificacion.Location = new System.Drawing.Point(214, 374);
            this.Calificacion.Name = "Calificacion";
            this.Calificacion.Size = new System.Drawing.Size(64, 13);
            this.Calificacion.TabIndex = 10;
            this.Calificacion.Text = "Calificación:";
            // 
            // NUDCalificacionMateriaCursada
            // 
            this.NUDCalificacionMateriaCursada.Location = new System.Drawing.Point(284, 371);
            this.NUDCalificacionMateriaCursada.Name = "NUDCalificacionMateriaCursada";
            this.NUDCalificacionMateriaCursada.Size = new System.Drawing.Size(54, 20);
            this.NUDCalificacionMateriaCursada.TabIndex = 11;
            // 
            // BTCursarMateria
            // 
            this.BTCursarMateria.Location = new System.Drawing.Point(308, 419);
            this.BTCursarMateria.Name = "BTCursarMateria";
            this.BTCursarMateria.Size = new System.Drawing.Size(94, 23);
            this.BTCursarMateria.TabIndex = 12;
            this.BTCursarMateria.Text = "Cursar Materia";
            this.BTCursarMateria.UseVisualStyleBackColor = true;
            this.BTCursarMateria.Click += new System.EventHandler(this.BTCursarMateria_Click);
            // 
            // LPromedio
            // 
            this.LPromedio.AutoSize = true;
            this.LPromedio.Location = new System.Drawing.Point(79, 513);
            this.LPromedio.Name = "LPromedio";
            this.LPromedio.Size = new System.Drawing.Size(31, 13);
            this.LPromedio.TabIndex = 14;
            this.LPromedio.Text = "........";
            // 
            // LCreditos
            // 
            this.LCreditos.AutoSize = true;
            this.LCreditos.Location = new System.Drawing.Point(200, 513);
            this.LCreditos.Name = "LCreditos";
            this.LCreditos.Size = new System.Drawing.Size(31, 13);
            this.LCreditos.TabIndex = 15;
            this.LCreditos.Text = "........";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 513);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Promedio: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(134, 513);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Creditos:";
            // 
            // FDatosAlumno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 597);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.LCreditos);
            this.Controls.Add(this.LPromedio);
            this.Controls.Add(this.BTCursarMateria);
            this.Controls.Add(this.NUDCalificacionMateriaCursada);
            this.Controls.Add(this.Calificacion);
            this.Controls.Add(this.TBClaveMateriaCursada);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.RTBDatosMateria);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.TB_NumControl);
            this.Controls.Add(this.TB_NombreAlumno);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FDatosAlumno";
            this.Text = "Datos del Alumno";
            ((System.ComponentModel.ISupportInitialize)(this.NUDCalificacionMateriaCursada)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TB_NombreAlumno;
        private System.Windows.Forms.TextBox TB_NumControl;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox RTBDatosMateria;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TBClaveMateriaCursada;
        private System.Windows.Forms.Label Calificacion;
        private System.Windows.Forms.NumericUpDown NUDCalificacionMateriaCursada;
        private System.Windows.Forms.Button BTCursarMateria;
        private System.Windows.Forms.Label LPromedio;
        private System.Windows.Forms.Label LCreditos;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}

