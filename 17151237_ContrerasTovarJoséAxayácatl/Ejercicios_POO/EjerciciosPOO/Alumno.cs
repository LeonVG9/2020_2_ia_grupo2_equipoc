﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjerciciosPOO
{
    class Alumno
    {
        string NumControl;
        string Nombre;
        int NumSemestre;
        string Carrera;
        int Promedio;
        int CreditosAcumulados;

        List<Materia> Materias;

        public Alumno(string NumControl, string Nombre)
        {
            this.NumControl = NumControl;
            this.Nombre = Nombre;
            NumSemestre = 1;
            Promedio = 0;
            CreditosAcumulados = 0;
            Materias = new List<Materia>();
        }

        public int CalcularPromedio()
        {
            if (Materias.Count == 0)
            {
                return 0;
            }
            else
            {
                int Sumatoria = 0;
                for (int i = 0; i < Materias.Count; i++)
                {
                    Sumatoria += Materias[i].GetCalificacion();
                }

                return Promedio = Sumatoria / Materias.Count;
            }
        }

        public void AgregarMateria(Materia MateriaNueva)
        {
            Materias.Add(MateriaNueva);
        }

        public string CursarMateria(string ClaveMateria, int Calificacion)
        {
            string error = "";
            for (int i = 0; i < Materias.Count; i++)
            {
                error = Materias[i].Cursar(ClaveMateria, Calificacion);
                if (error == "")
                    return "";

                if (error != "No es la materia.")
                    return error;
          
            }
            return "El alumno no esta matriculado en esta materia. ";
        }

        public int CalcularCreditos()
        {
            CreditosAcumulados = 0;
            for (int i = 0; i < Materias.Count; i++)
            {
                if (Materias[i].YaEstaAprobada())
                {
                    CreditosAcumulados += Materias[i].GetCreditos();
                }
            }

            return CreditosAcumulados;
        }

        public string GetDatosMaterias() {

            StringBuilder datos = new StringBuilder();

            for (int i = 0; i < Materias.Count; i++)
            {
                datos.Append(Materias[i].GetDatosMaterias());
            }
            return datos.ToString();
        }
    }
}
