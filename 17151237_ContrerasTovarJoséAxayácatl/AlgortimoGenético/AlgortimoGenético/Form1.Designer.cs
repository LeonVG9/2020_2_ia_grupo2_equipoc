﻿namespace AlgortimoGenético
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.LB_Modelo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TB_Modelo = new System.Windows.Forms.TextBox();
            this.NUP_Poblacion = new System.Windows.Forms.NumericUpDown();
            this.RTB_Poblacion = new System.Windows.Forms.RichTextBox();
            this.BTN_Poblacion = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.NUP_Poblacion)).BeginInit();
            this.SuspendLayout();
            // 
            // LB_Modelo
            // 
            this.LB_Modelo.AutoSize = true;
            this.LB_Modelo.Location = new System.Drawing.Point(24, 29);
            this.LB_Modelo.Name = "LB_Modelo";
            this.LB_Modelo.Size = new System.Drawing.Size(85, 13);
            this.LB_Modelo.TabIndex = 0;
            this.LB_Modelo.Text = "Modelo a seguir:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(187, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Número de individuos en la población:";
            // 
            // TB_Modelo
            // 
            this.TB_Modelo.Location = new System.Drawing.Point(137, 26);
            this.TB_Modelo.Name = "TB_Modelo";
            this.TB_Modelo.Size = new System.Drawing.Size(131, 20);
            this.TB_Modelo.TabIndex = 2;
            // 
            // NUP_Poblacion
            // 
            this.NUP_Poblacion.Location = new System.Drawing.Point(217, 63);
            this.NUP_Poblacion.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.NUP_Poblacion.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NUP_Poblacion.Name = "NUP_Poblacion";
            this.NUP_Poblacion.Size = new System.Drawing.Size(51, 20);
            this.NUP_Poblacion.TabIndex = 3;
            this.NUP_Poblacion.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // RTB_Poblacion
            // 
            this.RTB_Poblacion.Location = new System.Drawing.Point(27, 135);
            this.RTB_Poblacion.Name = "RTB_Poblacion";
            this.RTB_Poblacion.ReadOnly = true;
            this.RTB_Poblacion.Size = new System.Drawing.Size(184, 193);
            this.RTB_Poblacion.TabIndex = 4;
            this.RTB_Poblacion.Text = "";
            // 
            // BTN_Poblacion
            // 
            this.BTN_Poblacion.Location = new System.Drawing.Point(99, 95);
            this.BTN_Poblacion.Name = "BTN_Poblacion";
            this.BTN_Poblacion.Size = new System.Drawing.Size(112, 23);
            this.BTN_Poblacion.TabIndex = 5;
            this.BTN_Poblacion.Text = "Generar población";
            this.BTN_Poblacion.UseVisualStyleBackColor = true;
            this.BTN_Poblacion.Click += new System.EventHandler(this.BTN_Poblacion_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 354);
            this.Controls.Add(this.BTN_Poblacion);
            this.Controls.Add(this.RTB_Poblacion);
            this.Controls.Add(this.NUP_Poblacion);
            this.Controls.Add(this.TB_Modelo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LB_Modelo);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.NUP_Poblacion)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LB_Modelo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TB_Modelo;
        private System.Windows.Forms.NumericUpDown NUP_Poblacion;
        private System.Windows.Forms.RichTextBox RTB_Poblacion;
        private System.Windows.Forms.Button BTN_Poblacion;
    }
}

