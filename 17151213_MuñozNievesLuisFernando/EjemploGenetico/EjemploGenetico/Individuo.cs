﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemploGenetico
{
    class Individuo
    {
        int Fitness;
        string Genotipo;

        Random Aleatorio;
        EvaluadorGeneral Evaluador;

        public Individuo(EvaluadorGeneral Evaluador, Random Aleatorio)
        {
            this.Evaluador = Evaluador;
            Genotipo = "";
            this.Aleatorio = Aleatorio;

            for (int i = 0; i < Evaluador.TamGenotipo; i++)
            {
                Genotipo += Aleatorio.Next(Evaluador.LimInferiorFenotipo, Evaluador.LimSuperiorFenotipo + 1);
            }
        }

        public Individuo(EvaluadorGeneral Evaluador, Random Aleatorio, Individuo Padre, Individuo Madre)
        {
            this.Evaluador = Evaluador;
            this.Aleatorio = Aleatorio;

            int punto_cruzamiento = Aleatorio.Next(Evaluador.TamGenotipo);
            Genotipo = Padre.Genotipo.Substring(0, punto_cruzamiento);
            Genotipo += Madre.Genotipo.Substring(punto_cruzamiento);
        }

        public int GetFitness()
        {
            Fitness = Evaluador.Evaluar(this);
            return Fitness;
        }
        public string GetGenotipo()
        {
            return Genotipo;
        }

    }

}
