﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EjemploGenetico
{
    public partial class Form1 : Form
    {
        Poblacion LaPoblacion;

        public Form1()
        {
            InitializeComponent();
        }

        private void BTN_Inicio_Click(object sender, EventArgs e)
        {
            LaPoblacion = new Poblacion((int)NUD_Poblacion.Value);

            RTB_Poblacion.Text = LaPoblacion.GetGenotipoYFitness();
        }

        private void BTN_Cruzar_Click(object sender, EventArgs e)
        {
            LaPoblacion.Cruzamiento();

            RTB_Cruzamiento.Text = LaPoblacion.GetGenotipoYFitness();
        }

        private void BTN_Mutar_Click(object sender, EventArgs e)
        {

        }

        private void BTN_Seleccion_Click(object sender, EventArgs e)
        {
            RTB_NuevaPoblacion.Text = LaPoblacion.Seleccion((float)NUD_Presion.Value);
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
