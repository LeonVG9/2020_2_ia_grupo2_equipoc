﻿namespace EjerciciosPOO
{
    partial class FDatosAlumno
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TB_NombreAlumno = new System.Windows.Forms.TextBox();
            this.TB_NumControl = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.RTB_DatosMaterias = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TB_ClaveMateriaCursar = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.NUD_CalificacionMateriaCursada = new System.Windows.Forms.NumericUpDown();
            this.BTN_Cursar = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.TB_Promedio = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TB_Creditos = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_CalificacionMateriaCursada)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre del alumno";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Número de control";
            // 
            // TB_NombreAlumno
            // 
            this.TB_NombreAlumno.Location = new System.Drawing.Point(130, 6);
            this.TB_NombreAlumno.Name = "TB_NombreAlumno";
            this.TB_NombreAlumno.Size = new System.Drawing.Size(172, 20);
            this.TB_NombreAlumno.TabIndex = 2;
            // 
            // TB_NumControl
            // 
            this.TB_NumControl.Location = new System.Drawing.Point(130, 45);
            this.TB_NumControl.Name = "TB_NumControl";
            this.TB_NumControl.Size = new System.Drawing.Size(172, 20);
            this.TB_NumControl.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(322, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Ingresar Nuevo";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(322, 42);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(99, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Agregar Materia";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Materias";
            // 
            // RTB_DatosMaterias
            // 
            this.RTB_DatosMaterias.Location = new System.Drawing.Point(65, 97);
            this.RTB_DatosMaterias.Name = "RTB_DatosMaterias";
            this.RTB_DatosMaterias.ReadOnly = true;
            this.RTB_DatosMaterias.Size = new System.Drawing.Size(295, 96);
            this.RTB_DatosMaterias.TabIndex = 7;
            this.RTB_DatosMaterias.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 212);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Materia a cursar";
            // 
            // TB_ClaveMateriaCursar
            // 
            this.TB_ClaveMateriaCursar.Location = new System.Drawing.Point(101, 209);
            this.TB_ClaveMateriaCursar.Name = "TB_ClaveMateriaCursar";
            this.TB_ClaveMateriaCursar.Size = new System.Drawing.Size(77, 20);
            this.TB_ClaveMateriaCursar.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(196, 212);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Calificación";
            // 
            // NUD_CalificacionMateriaCursada
            // 
            this.NUD_CalificacionMateriaCursada.Location = new System.Drawing.Point(263, 210);
            this.NUD_CalificacionMateriaCursada.Name = "NUD_CalificacionMateriaCursada";
            this.NUD_CalificacionMateriaCursada.Size = new System.Drawing.Size(55, 20);
            this.NUD_CalificacionMateriaCursada.TabIndex = 11;
            // 
            // BTN_Cursar
            // 
            this.BTN_Cursar.Location = new System.Drawing.Point(348, 209);
            this.BTN_Cursar.Name = "BTN_Cursar";
            this.BTN_Cursar.Size = new System.Drawing.Size(63, 23);
            this.BTN_Cursar.TabIndex = 12;
            this.BTN_Cursar.Text = "Cursar";
            this.BTN_Cursar.UseVisualStyleBackColor = true;
            this.BTN_Cursar.Click += new System.EventHandler(this.BTN_Cursar_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 251);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Promedio";
            // 
            // TB_Promedio
            // 
            this.TB_Promedio.Enabled = false;
            this.TB_Promedio.Location = new System.Drawing.Point(69, 248);
            this.TB_Promedio.Name = "TB_Promedio";
            this.TB_Promedio.ReadOnly = true;
            this.TB_Promedio.Size = new System.Drawing.Size(60, 20);
            this.TB_Promedio.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(152, 251);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Créditos";
            // 
            // TB_Creditos
            // 
            this.TB_Creditos.Enabled = false;
            this.TB_Creditos.Location = new System.Drawing.Point(203, 248);
            this.TB_Creditos.Name = "TB_Creditos";
            this.TB_Creditos.ReadOnly = true;
            this.TB_Creditos.Size = new System.Drawing.Size(60, 20);
            this.TB_Creditos.TabIndex = 16;
            // 
            // FDatosAlumno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 284);
            this.Controls.Add(this.TB_Creditos);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.TB_Promedio);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.BTN_Cursar);
            this.Controls.Add(this.NUD_CalificacionMateriaCursada);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TB_ClaveMateriaCursar);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.RTB_DatosMaterias);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.TB_NumControl);
            this.Controls.Add(this.TB_NombreAlumno);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FDatosAlumno";
            this.Text = "Datos del Alumno";
            ((System.ComponentModel.ISupportInitialize)(this.NUD_CalificacionMateriaCursada)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TB_NombreAlumno;
        private System.Windows.Forms.TextBox TB_NumControl;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox RTB_DatosMaterias;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TB_ClaveMateriaCursar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown NUD_CalificacionMateriaCursada;
        private System.Windows.Forms.Button BTN_Cursar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TB_Promedio;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TB_Creditos;
    }
}

