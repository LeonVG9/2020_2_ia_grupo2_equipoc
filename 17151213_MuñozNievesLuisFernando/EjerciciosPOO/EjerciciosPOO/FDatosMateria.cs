﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EjerciciosPOO
{
    public partial class FDatosMateria : Form
    {
        Materia LaMateria;
        public FDatosMateria()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LaMateria = new Materia(TB_NombreMateria.Text, TB_ClaveMateria.Text, (int)NUD_Creditos.Value);
            DialogResult = DialogResult.OK;
        }

        public Materia GetMateria()
        {
            return LaMateria;
        }
    }
}
