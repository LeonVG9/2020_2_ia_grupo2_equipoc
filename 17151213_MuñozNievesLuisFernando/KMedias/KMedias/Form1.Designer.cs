﻿namespace KMedias
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.aRCHIVOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aBRIRToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OFDTexto = new System.Windows.Forms.OpenFileDialog();
            this.RTBTextoArchivo = new System.Windows.Forms.RichTextBox();
            this.LabelTitulo = new System.Windows.Forms.Label();
            this.LabelA = new System.Windows.Forms.Label();
            this.LabelB = new System.Windows.Forms.Label();
            this.NUD_A = new System.Windows.Forms.NumericUpDown();
            this.NUD_B = new System.Windows.Forms.NumericUpDown();
            this.LabelDistancia = new System.Windows.Forms.Label();
            this.TB_Resultado = new System.Windows.Forms.TextBox();
            this.BTN_Calcular = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_B)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aRCHIVOToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(586, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // aRCHIVOToolStripMenuItem
            // 
            this.aRCHIVOToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aBRIRToolStripMenuItem});
            this.aRCHIVOToolStripMenuItem.Name = "aRCHIVOToolStripMenuItem";
            this.aRCHIVOToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.aRCHIVOToolStripMenuItem.Text = "ARCHIVO";
            // 
            // aBRIRToolStripMenuItem
            // 
            this.aBRIRToolStripMenuItem.Name = "aBRIRToolStripMenuItem";
            this.aBRIRToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.aBRIRToolStripMenuItem.Text = "ABRIR";
            this.aBRIRToolStripMenuItem.Click += new System.EventHandler(this.aBRIRToolStripMenuItem_Click);
            // 
            // OFDTexto
            // 
            this.OFDTexto.FileName = "openFileDialog1";
            this.OFDTexto.Filter = "Archivos de texto|*.txt|Valores separados por coma|*.csv|Todos los archivos|*.*";
            // 
            // RTBTextoArchivo
            // 
            this.RTBTextoArchivo.Location = new System.Drawing.Point(12, 41);
            this.RTBTextoArchivo.Name = "RTBTextoArchivo";
            this.RTBTextoArchivo.ReadOnly = true;
            this.RTBTextoArchivo.Size = new System.Drawing.Size(332, 248);
            this.RTBTextoArchivo.TabIndex = 1;
            this.RTBTextoArchivo.Text = "";
            // 
            // LabelTitulo
            // 
            this.LabelTitulo.AutoSize = true;
            this.LabelTitulo.Location = new System.Drawing.Point(389, 41);
            this.LabelTitulo.Name = "LabelTitulo";
            this.LabelTitulo.Size = new System.Drawing.Size(111, 13);
            this.LabelTitulo.TabIndex = 2;
            this.LabelTitulo.Text = "Selecciona los puntos";
            // 
            // LabelA
            // 
            this.LabelA.AutoSize = true;
            this.LabelA.Location = new System.Drawing.Point(359, 94);
            this.LabelA.Name = "LabelA";
            this.LabelA.Size = new System.Drawing.Size(45, 13);
            this.LabelA.TabIndex = 3;
            this.LabelA.Text = "Punto A";
            this.LabelA.Click += new System.EventHandler(this.LabelA_Click);
            // 
            // LabelB
            // 
            this.LabelB.AutoSize = true;
            this.LabelB.Location = new System.Drawing.Point(359, 143);
            this.LabelB.Name = "LabelB";
            this.LabelB.Size = new System.Drawing.Size(45, 13);
            this.LabelB.TabIndex = 4;
            this.LabelB.Text = "Punto B";
            // 
            // NUD_A
            // 
            this.NUD_A.Location = new System.Drawing.Point(432, 92);
            this.NUD_A.Name = "NUD_A";
            this.NUD_A.Size = new System.Drawing.Size(120, 20);
            this.NUD_A.TabIndex = 5;
            // 
            // NUD_B
            // 
            this.NUD_B.Location = new System.Drawing.Point(432, 141);
            this.NUD_B.Name = "NUD_B";
            this.NUD_B.Size = new System.Drawing.Size(120, 20);
            this.NUD_B.TabIndex = 6;
            // 
            // LabelDistancia
            // 
            this.LabelDistancia.AutoSize = true;
            this.LabelDistancia.Location = new System.Drawing.Point(359, 241);
            this.LabelDistancia.Name = "LabelDistancia";
            this.LabelDistancia.Size = new System.Drawing.Size(51, 13);
            this.LabelDistancia.TabIndex = 7;
            this.LabelDistancia.Text = "Distancia";
            // 
            // TB_Resultado
            // 
            this.TB_Resultado.Location = new System.Drawing.Point(432, 238);
            this.TB_Resultado.Name = "TB_Resultado";
            this.TB_Resultado.ReadOnly = true;
            this.TB_Resultado.Size = new System.Drawing.Size(100, 20);
            this.TB_Resultado.TabIndex = 8;
            this.TB_Resultado.TextChanged += new System.EventHandler(this.TB_Resultado_TextChanged);
            // 
            // BTN_Calcular
            // 
            this.BTN_Calcular.Location = new System.Drawing.Point(402, 189);
            this.BTN_Calcular.Name = "BTN_Calcular";
            this.BTN_Calcular.Size = new System.Drawing.Size(75, 23);
            this.BTN_Calcular.TabIndex = 9;
            this.BTN_Calcular.Text = "Calcular";
            this.BTN_Calcular.UseVisualStyleBackColor = true;
            this.BTN_Calcular.Click += new System.EventHandler(this.BTN_Calcular_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 312);
            this.Controls.Add(this.BTN_Calcular);
            this.Controls.Add(this.TB_Resultado);
            this.Controls.Add(this.LabelDistancia);
            this.Controls.Add(this.NUD_B);
            this.Controls.Add(this.NUD_A);
            this.Controls.Add(this.LabelB);
            this.Controls.Add(this.LabelA);
            this.Controls.Add(this.LabelTitulo);
            this.Controls.Add(this.RTBTextoArchivo);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_B)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem aRCHIVOToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aBRIRToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog OFDTexto;
        private System.Windows.Forms.RichTextBox RTBTextoArchivo;
        private System.Windows.Forms.Label LabelTitulo;
        private System.Windows.Forms.Label LabelA;
        private System.Windows.Forms.Label LabelB;
        private System.Windows.Forms.NumericUpDown NUD_A;
        private System.Windows.Forms.NumericUpDown NUD_B;
        private System.Windows.Forms.Label LabelDistancia;
        private System.Windows.Forms.TextBox TB_Resultado;
        private System.Windows.Forms.Button BTN_Calcular;
    }
}

