﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemploGenetico
{
    class EvaluadorTipo1 : EvaluadorGeneral
    {
   
        string Modelo;

        public EvaluadorTipo1(string Modelo)
        {
            this.Modelo = Modelo;

            TamGenotipo = 8;
            LimInferiorFenotipo = 1;
            LimSuperiorFenotipo = 8;
        }

        public override int Evaluar(Individuo individuo)
        {
            string genotipo = individuo.GetGenotipo();
            int contador = 0;

            for (int i = 0; i < genotipo.Length; i++)
            {
                if (genotipo[i] == Modelo[i])
                {
                    contador++;
                }
            }

            return contador;
        }
    }
}
