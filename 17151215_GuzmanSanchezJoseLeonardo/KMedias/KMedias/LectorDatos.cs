﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace KMedias
{
    class LectorDatos
    {
        public int columnas = 0;
        public int filas = 0;
        public string[] variables;
        string Texto;
        public double[,] Valores;
        public string[] registros;

        public LectorDatos(string pathFuente = "")
        {
            //Se asegura de que se haya seleccionado un archivo//
            if (pathFuente.Length > 0)
            {
                //Manda el archivo al método Leer Datos
                LeerDatos(pathFuente);
            }
        }

        public void LeerDatos(string pathFuente)
        {
            //En el arreglo registros se guardan todas las lineas del archivo//
            registros = File.ReadAllLines(pathFuente);

            //En este foreach, se recorre el arreglo registros linea por linea hasta encontrar una coma//
            //y lo guarda en el arreglo variables
            //La variable columnas nos dice cuantos atributos o variables tiene//
            //La variable filas obtiene el total de registros.

            foreach (var LineaActual in registros)
            {
                variables = LineaActual.Split(',');
                columnas = variables.GetLength(0);
                filas++;
            }

            //Con filas y columnas, formamos la matriz valores, que tendra todos los datos númericos del archivo//
            //Reiniciamos filas para volver a hacer el conteo
            Valores = new double[filas, columnas];
            filas = 0;

            //Vamos a recorrer otra vez el arreglo registro, hasta encontrar la coma
            //Ahora, vamos a ir introduciendo en la matriz valores, los datos que hay en el arreglo variables
            //Asi hasta terminar con las filas y columnas
            foreach (var LineaActual in registros)
            {
                variables = LineaActual.Split(',');
                for (int m = 0; m < variables.GetLength(0); m++)
                {
                    Valores[filas, m] = Convert.ToDouble(variables[m]);
                }

                filas++;
            }

            //Imprimimos valores
            for (int i = 0; i < Valores.GetLength(0); i++)
            {
                for (int j = 0; j < Valores.GetLength(1); j++)
                {
                    Texto += Valores[i, j] + " ";
                }

                Texto += "\n";
            }

        }

        //Regresa la matriz impresa
        public string GetTexto()
        {
            return Texto;
        }

        public string GetColumnas()
        {
            return columnas.ToString();
        }
    }
}