﻿namespace KMedias
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.aRCHIVOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aBRIRToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OFDTexto = new System.Windows.Forms.OpenFileDialog();
            this.RTBTextoArchivo = new System.Windows.Forms.RichTextBox();
            this.BTN_Calcular = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.NUD_Cluster = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.LabelTitulo = new System.Windows.Forms.Label();
            this.BTN_IngresaCluster = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.Label_columnas = new System.Windows.Forms.Label();
            this.RTB_Cluster = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_Cluster)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aRCHIVOToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(772, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // aRCHIVOToolStripMenuItem
            // 
            this.aRCHIVOToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aBRIRToolStripMenuItem});
            this.aRCHIVOToolStripMenuItem.Name = "aRCHIVOToolStripMenuItem";
            this.aRCHIVOToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.aRCHIVOToolStripMenuItem.Text = "ARCHIVO";
            // 
            // aBRIRToolStripMenuItem
            // 
            this.aBRIRToolStripMenuItem.Name = "aBRIRToolStripMenuItem";
            this.aBRIRToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.aBRIRToolStripMenuItem.Text = "ABRIR";
            this.aBRIRToolStripMenuItem.Click += new System.EventHandler(this.aBRIRToolStripMenuItem_Click);
            // 
            // OFDTexto
            // 
            this.OFDTexto.FileName = "openFileDialog1";
            this.OFDTexto.Filter = "Valores separados por coma|*.csv";
            // 
            // RTBTextoArchivo
            // 
            this.RTBTextoArchivo.Location = new System.Drawing.Point(12, 66);
            this.RTBTextoArchivo.Name = "RTBTextoArchivo";
            this.RTBTextoArchivo.ReadOnly = true;
            this.RTBTextoArchivo.Size = new System.Drawing.Size(226, 206);
            this.RTBTextoArchivo.TabIndex = 1;
            this.RTBTextoArchivo.Text = "";
            // 
            // BTN_Calcular
            // 
            this.BTN_Calcular.Enabled = false;
            this.BTN_Calcular.Location = new System.Drawing.Point(588, 314);
            this.BTN_Calcular.Name = "BTN_Calcular";
            this.BTN_Calcular.Size = new System.Drawing.Size(98, 23);
            this.BTN_Calcular.TabIndex = 9;
            this.BTN_Calcular.Text = "Calcular";
            this.BTN_Calcular.UseVisualStyleBackColor = true;
            this.BTN_Calcular.Click += new System.EventHandler(this.BTN_Calcular_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(264, 121);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 16);
            this.label1.TabIndex = 10;
            this.label1.Text = "Ingresa el número de clústers";
            // 
            // NUD_Cluster
            // 
            this.NUD_Cluster.Enabled = false;
            this.NUD_Cluster.Location = new System.Drawing.Point(417, 121);
            this.NUD_Cluster.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NUD_Cluster.Name = "NUD_Cluster";
            this.NUD_Cluster.Size = new System.Drawing.Size(68, 20);
            this.NUD_Cluster.TabIndex = 11;
            this.NUD_Cluster.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(23, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 20);
            this.label2.TabIndex = 14;
            this.label2.Text = "Contenido del archivo";
            // 
            // LabelTitulo
            // 
            this.LabelTitulo.AutoSize = true;
            this.LabelTitulo.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelTitulo.Location = new System.Drawing.Point(328, 39);
            this.LabelTitulo.Name = "LabelTitulo";
            this.LabelTitulo.Size = new System.Drawing.Size(128, 20);
            this.LabelTitulo.TabIndex = 2;
            this.LabelTitulo.Text = "Clúster y centroides";
            this.LabelTitulo.Click += new System.EventHandler(this.LabelTitulo_Click);
            // 
            // BTN_IngresaCluster
            // 
            this.BTN_IngresaCluster.Enabled = false;
            this.BTN_IngresaCluster.Location = new System.Drawing.Point(321, 158);
            this.BTN_IngresaCluster.Name = "BTN_IngresaCluster";
            this.BTN_IngresaCluster.Size = new System.Drawing.Size(100, 23);
            this.BTN_IngresaCluster.TabIndex = 16;
            this.BTN_IngresaCluster.Text = "Ingresar cluster";
            this.BTN_IngresaCluster.UseVisualStyleBackColor = true;
            this.BTN_IngresaCluster.Click += new System.EventHandler(this.BTN_IngresaCluster_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(264, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 16);
            this.label4.TabIndex = 18;
            this.label4.Text = "Numero de columnas:";
            // 
            // Label_columnas
            // 
            this.Label_columnas.AutoSize = true;
            this.Label_columnas.Enabled = false;
            this.Label_columnas.Location = new System.Drawing.Point(382, 80);
            this.Label_columnas.Name = "Label_columnas";
            this.Label_columnas.Size = new System.Drawing.Size(16, 13);
            this.Label_columnas.TabIndex = 19;
            this.Label_columnas.Text = "---";
            // 
            // RTB_Cluster
            // 
            this.RTB_Cluster.Enabled = false;
            this.RTB_Cluster.Location = new System.Drawing.Point(526, 66);
            this.RTB_Cluster.Name = "RTB_Cluster";
            this.RTB_Cluster.ReadOnly = true;
            this.RTB_Cluster.Size = new System.Drawing.Size(226, 206);
            this.RTB_Cluster.TabIndex = 20;
            this.RTB_Cluster.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(584, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 20);
            this.label3.TabIndex = 21;
            this.label3.Text = "Clusterización";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(772, 352);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.RTB_Cluster);
            this.Controls.Add(this.Label_columnas);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.BTN_IngresaCluster);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.NUD_Cluster);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BTN_Calcular);
            this.Controls.Add(this.LabelTitulo);
            this.Controls.Add(this.RTBTextoArchivo);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "KMeans";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_Cluster)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem aRCHIVOToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aBRIRToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog OFDTexto;
        private System.Windows.Forms.RichTextBox RTBTextoArchivo;
        private System.Windows.Forms.Button BTN_Calcular;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown NUD_Cluster;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label LabelTitulo;
        private System.Windows.Forms.Button BTN_IngresaCluster;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label Label_columnas;
        private System.Windows.Forms.RichTextBox RTB_Cluster;
        private System.Windows.Forms.Label label3;
    }
}

