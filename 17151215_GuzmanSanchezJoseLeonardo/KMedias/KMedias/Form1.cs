﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KMedias
{
    public partial class Form1 : Form
    {
        LectorDatos miLectorDatos;
        CalcularDistancia distancia;
        TextBox[,] tb;
        double[,] Cluster;

        public Form1()
        {
            InitializeComponent();
        }

        private void aBRIRToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Manda la ruta o el archivo con el que se va a trabajar//
            if (OFDTexto.ShowDialog() == DialogResult.OK)
            {
                miLectorDatos = new LectorDatos(OFDTexto.FileName);

                RTBTextoArchivo.Text = miLectorDatos.GetTexto();
                Label_columnas.Text = miLectorDatos.GetColumnas();

                NUD_Cluster.Enabled = true;
                BTN_IngresaCluster.Enabled = true;
            }
        }

        private void LabelA_Click(object sender, EventArgs e)
        {

        }

        private void TB_Resultado_TextChanged(object sender, EventArgs e)
        {

        }

        private void BTN_Calcular_Click(object sender, EventArgs e)
        {
            int numcluster = Convert.ToInt32(NUD_Cluster.Value);
            int numcolumna = Convert.ToInt32(miLectorDatos.GetColumnas());

            Cluster = new double[numcluster, numcolumna];

            for (int i = 0; i < numcluster; i++)
            {
                for (int j = 0; j < numcolumna; j++)
                {
                    Cluster[i, j] = Convert.ToDouble(tb[i,j].Text);
                }
            }

        }

        private void LabelTitulo_Click(object sender, EventArgs e)
        {

        }

        private void BTN_IngresaCluster_Click(object sender, EventArgs e)
        {
            int numcluster = Convert.ToInt32(NUD_Cluster.Value);
            int numcolumna = Convert.ToInt32(miLectorDatos.GetColumnas());
            int espacioX = 60;
            int espacioY = 50;
            BTN_Calcular.Enabled = true;

            tb = new TextBox[numcluster, numcolumna];

            for (int i = 0; i < numcluster; i++)
            {
                Label eti1 = new Label();
                eti1.Text = "Cluster " + (i+1).ToString();
                eti1.Name = "Label " + (i+1).ToString();
                eti1.Location = new Point((267), (130 + espacioY));
                Controls.Add(eti1);

                for (int j = 0; j < numcolumna; j++)
                {
                    tb[i, j] = new TextBox();
                    tb[i,j].Width = 50;
                    tb[i,j].Location = new Point((207+espacioX), (160+espacioY));
                    Controls.Add(tb[i,j]);
                    espacioX += 60;
                }
                espacioX = 60;
                espacioY += 70;
            }
        }

        private void CB_Centros_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
    }
}
