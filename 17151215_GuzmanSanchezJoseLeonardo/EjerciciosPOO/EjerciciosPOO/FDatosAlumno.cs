﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EjerciciosPOO
{
    public partial class FDatosAlumno : Form
    {
        Alumno ElAlumno;
        public FDatosAlumno()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ElAlumno = new Alumno(TBNumControl.Text,TBNombreAlumno.Text);
            button2.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FDatosMateria ventanaDatosMateria = new FDatosMateria();
            if (ventanaDatosMateria.ShowDialog() == DialogResult.OK)
            {
                ElAlumno.AgregarMateria(ventanaDatosMateria.GetMateria());
                RTBDatosMaterias.Text = ElAlumno.GetDatosMaterias();
            }
        }



  

        private void BCursarMateria_Click(object sender, EventArgs e)
        {
            string error = ElAlumno.CursarMateria(TBClaveMateriaCursada.Text, (int)NUDCalificacionMateriaCursada.Value);
            if (error == "")
            {
                MessageBox.Show("Materia cursada correctamente");
                LPromedio.Text = "Promedio: " + ElAlumno.CalcularPromedio();
                LCreditos.Text = "Creditos: " + ElAlumno.CalcularCreditos();
            }
            else
                MessageBox.Show("Error al cursar la materia: " + error);



        }
    }
}
