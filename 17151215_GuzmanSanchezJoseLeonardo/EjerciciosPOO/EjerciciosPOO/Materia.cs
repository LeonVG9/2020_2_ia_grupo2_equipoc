﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjerciciosPOO
{
    public static class MensajesErrores
    {
        public const string MateriaYaAprobada = "Ya se aprobó la materia anteriormente";
        public const string ClaveMateriaNoCoincide = "No es la materia";

    }
    public class Materia
    {
        string Clave;
        string Nombre;
        string Profesor;
        int Semestre;
        int Creditos;
        int Calificacion;

        int Intentos;


        public Materia(string Clave, string Nombre, int Creditos)
        {
            this.Clave = Clave;
            this.Nombre = Nombre;
            this.Creditos = Creditos;
            Calificacion = 0;
            Intentos = 1;
        }

        public string Cursar (string ClaveCursoMateria,int Calificacion)
        {
            if (ClaveCursoMateria != Clave)
                return MensajesErrores.ClaveMateriaNoCoincide;
            if (YaEstaAprobada())
                return MensajesErrores.MateriaYaAprobada;

            if (OportunidadesAgotadas())
                return "Oportunidades agotadas";

            if (Calificacion < 70)
            {
                Calificacion = 0;
                Intentos++;
            }
            else
            {
                this.Calificacion = Calificacion;
            }

            return "";
        }

        public bool YaEstaAprobada()
        {
            return Calificacion != 0;
        }

        public bool YaCursoLaMateria()
        {
            return Intentos > 1 || Calificacion != 0;
        }

        public bool OportunidadesAgotadas()
        {
            return Intentos > 3;
        }

        public int GetCalificacion() {
            return Calificacion;
        }

        public int GetCreditos() {
            return Creditos;
        }
        public string GetDatosMateria() {
            string datos = "";
            datos += Nombre + ", clave: " + Clave + ", creditos: " + Creditos;
            return datos;
        }



    }
}
