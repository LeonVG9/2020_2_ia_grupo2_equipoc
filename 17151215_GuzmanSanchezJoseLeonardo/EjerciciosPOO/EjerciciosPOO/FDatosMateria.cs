﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EjerciciosPOO
{
    public partial class FDatosMateria : Form
    {
        Materia LaMateria=null;
        public FDatosMateria()
        {
            InitializeComponent();
        }

        private void Cancelar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void Aceptar_Click(object sender, EventArgs e)
        {
            LaMateria = new Materia(TBClaveMateria.Text, TBNombreMateria.Text, (int)NUDCreditos.Value);
            DialogResult = DialogResult.OK;
        }

        public Materia GetMateria() {
            return LaMateria;
        }
    }
}
