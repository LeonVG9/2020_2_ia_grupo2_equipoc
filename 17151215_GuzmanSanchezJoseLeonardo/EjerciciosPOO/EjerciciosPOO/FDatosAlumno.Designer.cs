﻿namespace EjerciciosPOO
{
    partial class FDatosAlumno
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TBNombreAlumno = new System.Windows.Forms.TextBox();
            this.TBNumControl = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.RTBDatosMaterias = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TBClaveMateriaCursada = new System.Windows.Forms.TextBox();
            this.NUDCalif = new System.Windows.Forms.Label();
            this.NUDCalificacionMateriaCursada = new System.Windows.Forms.NumericUpDown();
            this.BCursarMateria = new System.Windows.Forms.Button();
            this.LPromedio = new System.Windows.Forms.Label();
            this.LCreditos = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.NUDCalificacionMateriaCursada)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre del alumno";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Numero de control";
            // 
            // TBNombreAlumno
            // 
            this.TBNombreAlumno.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBNombreAlumno.Location = new System.Drawing.Point(136, 17);
            this.TBNombreAlumno.Name = "TBNombreAlumno";
            this.TBNombreAlumno.Size = new System.Drawing.Size(377, 20);
            this.TBNombreAlumno.TabIndex = 2;
            // 
            // TBNumControl
            // 
            this.TBNumControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBNumControl.Location = new System.Drawing.Point(136, 47);
            this.TBNumControl.Name = "TBNumControl";
            this.TBNumControl.Size = new System.Drawing.Size(377, 20);
            this.TBNumControl.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(243, 73);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(95, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Inscribir alumno";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(243, 102);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(95, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Agregar materia";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // RTBDatosMaterias
            // 
            this.RTBDatosMaterias.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RTBDatosMaterias.Location = new System.Drawing.Point(108, 143);
            this.RTBDatosMaterias.Name = "RTBDatosMaterias";
            this.RTBDatosMaterias.ReadOnly = true;
            this.RTBDatosMaterias.Size = new System.Drawing.Size(405, 103);
            this.RTBDatosMaterias.TabIndex = 6;
            this.RTBDatosMaterias.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Materias";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 290);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Materia a cursar";
            // 
            // TBClaveMateriaCursada
            // 
            this.TBClaveMateriaCursada.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBClaveMateriaCursada.Location = new System.Drawing.Point(108, 287);
            this.TBClaveMateriaCursada.Name = "TBClaveMateriaCursada";
            this.TBClaveMateriaCursada.Size = new System.Drawing.Size(96, 20);
            this.TBClaveMateriaCursada.TabIndex = 9;
            // 
            // NUDCalif
            // 
            this.NUDCalif.AutoSize = true;
            this.NUDCalif.Location = new System.Drawing.Point(243, 290);
            this.NUDCalif.Name = "NUDCalif";
            this.NUDCalif.Size = new System.Drawing.Size(61, 13);
            this.NUDCalif.TabIndex = 10;
            this.NUDCalif.Text = "Calificación";
            // 
            // NUDCalificacionMateriaCursada
            // 
            this.NUDCalificacionMateriaCursada.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NUDCalificacionMateriaCursada.Location = new System.Drawing.Point(310, 287);
            this.NUDCalificacionMateriaCursada.Name = "NUDCalificacionMateriaCursada";
            this.NUDCalificacionMateriaCursada.Size = new System.Drawing.Size(61, 20);
            this.NUDCalificacionMateriaCursada.TabIndex = 11;
            // 
            // BCursarMateria
            // 
            this.BCursarMateria.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BCursarMateria.Location = new System.Drawing.Point(422, 287);
            this.BCursarMateria.Name = "BCursarMateria";
            this.BCursarMateria.Size = new System.Drawing.Size(89, 23);
            this.BCursarMateria.TabIndex = 12;
            this.BCursarMateria.Text = "Cursar materia";
            this.BCursarMateria.UseVisualStyleBackColor = true;
            this.BCursarMateria.Click += new System.EventHandler(this.BCursarMateria_Click);
            // 
            // LPromedio
            // 
            this.LPromedio.AutoSize = true;
            this.LPromedio.Location = new System.Drawing.Point(16, 357);
            this.LPromedio.Name = "LPromedio";
            this.LPromedio.Size = new System.Drawing.Size(51, 13);
            this.LPromedio.TabIndex = 14;
            this.LPromedio.Text = "Promedio";
            // 
            // LCreditos
            // 
            this.LCreditos.AutoSize = true;
            this.LCreditos.Location = new System.Drawing.Point(122, 354);
            this.LCreditos.Name = "LCreditos";
            this.LCreditos.Size = new System.Drawing.Size(45, 13);
            this.LCreditos.TabIndex = 15;
            this.LCreditos.Text = "Creditos";
            // 
            // FDatosAlumno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 379);
            this.Controls.Add(this.LCreditos);
            this.Controls.Add(this.LPromedio);
            this.Controls.Add(this.BCursarMateria);
            this.Controls.Add(this.NUDCalificacionMateriaCursada);
            this.Controls.Add(this.NUDCalif);
            this.Controls.Add(this.TBClaveMateriaCursada);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.RTBDatosMaterias);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.TBNumControl);
            this.Controls.Add(this.TBNombreAlumno);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FDatosAlumno";
            this.Text = "Datos del alumno";
            ((System.ComponentModel.ISupportInitialize)(this.NUDCalificacionMateriaCursada)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TBNombreAlumno;
        private System.Windows.Forms.TextBox TBNumControl;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.RichTextBox RTBDatosMaterias;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TBClaveMateriaCursada;
        private System.Windows.Forms.Label NUDCalif;
        private System.Windows.Forms.NumericUpDown NUDCalificacionMateriaCursada;
        private System.Windows.Forms.Button BCursarMateria;
        private System.Windows.Forms.Label LPromedio;
        private System.Windows.Forms.Label LCreditos;
    }
}

