﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjerciciosPOO
{
    

    class Alumno
    {
        string NumControl;
        string Nombre;
        int NumSemestre;
        string Carrera;
        int Promedio;
        int CreditosAcumulados;
        List<Materia> Materias;

        public Alumno(string NumControl, string Nombre )
        {
            this.NumControl = NumControl;
            this.Nombre = Nombre;
            NumSemestre = 1;
            Promedio = 0;
            CreditosAcumulados = 0;
            Materias = new List<Materia>();
        }

        public int CalcularPromedio() {
            if (Materias.Count == 0)
                return 0;
            int sumatoria = 0;
            for (int i = 0; i < Materias.Count; i++)
            {
                sumatoria += Materias[i].GetCalificacion();
            }

            Promedio = sumatoria / Materias.Count;

            return Promedio;
        }

        public void AgregarMateria(Materia MateriaNueva) {
            Materias.Add(MateriaNueva);
        }

        public string CursarMateria(string ClaveMateria, int Calificacion) {
            string error = "";
            for (int i = 0; i < Materias.Count; i++)
            {
                error = Materias[i].Cursar(ClaveMateria, Calificacion);
                if (error == "")
                    return "";
                if (error != MensajesErrores.ClaveMateriaNoCoincide)
                    return error;

            }
            return "El alumno no está matriculado en esta materia";
        }

        public int CalcularCreditos() {
            CreditosAcumulados = 0;
            for (int i = 0; i < Materias.Count; i++)
            {
                if (Materias[i].YaEstaAprobada())
                    CreditosAcumulados += Materias[i].GetCreditos();
            }
            return CreditosAcumulados;

        }

        public string GetDatosMaterias() {
            StringBuilder datos = new StringBuilder();
            for (int i = 0; i < Materias.Count; i++)
            {
                datos.Append(Materias[i].GetDatosMateria()+"\n");
            }

            return datos.ToString();
        }



    }
}
